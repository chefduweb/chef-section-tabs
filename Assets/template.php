<?php
	
	if( $section->hide_title === 'false' )
		$section->theTitle();
			
		//echo cuisine_dump($section);

		
		echo '<div class="section-container-wrapper '.esc_attr( $section->slug ).'">';
			
			echo '<div class="section-container-labels">';

				echo '<ul>';

					$i = 0;
					foreach( $section->sections->all() as $subSection ){


						echo '<li data-content="'.$subSection->id.'"';
							if( $i == 0 ) echo ' class="active"';
						echo '>';

							echo $section->getTabTitle( $subSection );

						echo '</li>';

						$i++;

					}



				echo '</ul>';

			echo '</div>';

			echo '<div class="section-container">';
				the_containered_sections( $section );
			echo '</div>';

		echo '</div>';

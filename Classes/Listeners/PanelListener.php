<?php

	namespace ChefSectionTabs\Listeners;

	use Cuisine\Wrappers\Field;
	use ChefSectionTabs\Wrappers\StaticInstance;
	use ChefSections\Wrappers\SettingsPanel as Panel;

	class PanelListener extends StaticInstance{


		/**
		 * Init admin events & vars
		 */
		public function __construct(){

			$this->listen();

		}

		/**
		 * Listen for SettingsPanels
		 * 
		 * @return void
		 */
		private function listen(){


			add_action( 'init', function(){

				$fields = $this->getSectionSettingFields();

				Panel::make( 
					__( 'Tab settings', 'chefsections' ),
					'tab-settings',
					array(
						'position' 		=> 2,
						'icon'			=> 'dashicons-index-card',
						'rules' 		=> [ 'inContainer' => 'tabs' ]
					)

				)->set( $fields );

			});
		}

		/**
		 * Returns an array of field objects
		 * 
		 * @return array
		 */
		private function getSectionSettingFields(){
			
			$fields = array(

				Field::text(
					'tabTitle',
					__( 'Tab title', 'chefsectiontabs' )
				),

				Field::image( 
					'tabBg',
					__( 'Background', 'chefsectiontabs' )
				)

			);

			return $fields;
		}

	}

	\ChefSectionTabs\Listeners\PanelListener::getInstance();

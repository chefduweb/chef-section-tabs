<?php

	namespace ChefSectionTabs\Listeners;

	use \Cuisine\Utilities\Url;
	use \ChefSectionTabs\Wrappers\StaticInstance;
	use \ChefSections\Collections\InContainerCollection;

	class EventListeners extends StaticInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			$this->listen();

		}

		/**
		 * Listen for container events
		 * 
		 * @return void
		 */
		public function listen()
		{

			/**
			 * Add the container
			 */
			add_filter( 'chef_sections_containers', function( $data ){

				$template = Url::path( 'plugin', 'chef-section-tabs/Assets/template.php', false );

				$data[ 'tabs' ] = [
					'label' 	=> 'Section Tabs',
					'view' 		=> 'tabbed',
					'template' 	=> $template,
					'class' 	=> '\ChefSectionTabs\Hooks\TabbedContainerSection'
				];
				
				return $data;
			});


			/**
			 * Set Section Classes
			 */
			add_filter( 'chef_section_classes', function( $class, $section ){

				if( !is_null( $section->container_id ) ){

					$collection = new InContainerCollection( $section->post_id, $section->container_id );
					$first = $collection->toArray()->first();

					$class .= ' tab-content-'.$section->id;

					if( $first['id'] == $section->id )
						$class .= ' active';

				}

				return $class;

			}, 100, 2 );

		}



	}

	\ChefSectionTabs\Listeners\EventListeners::getInstance();

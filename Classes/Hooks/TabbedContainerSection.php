<?php 
	
	namespace ChefSectionTabs\Hooks;

	use ChefSections\SectionTypes\Container;

	class TabbedContainerSection extends Container{

		/**
		 * Return a tabbed view
		 * 
		 * @return string
		 */
		public function getView()
		{
			return 'tabbed';
		}


		/**
		 * Return the tab title for this section
		 * 
		 * @param  ChefSections\SectionTypes\BaseSection $section
		 * 
		 * @return String
		 */
		public function getTabTitle( $section ){

			$tabTitle = $section->getProperty( 'tabTitle', false );

			if( $tabTitle != false && $tabTitle != '' )
				return $tabTitle;

			return $section->title;

		}
	}